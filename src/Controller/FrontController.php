<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Service\CashIdService;

class FrontController extends AbstractController
{
    /**
     * @Route("/")
     */
    function index(RequestStack $requestStack, CashIdService $CashId)
    {
        $session = $requestStack->getSession();
        $challenge = $session->get('cashid_challenge', "");

        if ($challenge === "") {
            $challenge = $CashId->create_request("login", $session->getId());
        }

        $loginData = ["loggedIn" => $session->get('loggedIn'), "address" => $session->get('address')];

        return $this->render('front/front.html.twig', ["challenge" => $challenge, "loginData" => $loginData]);

    }

    /**
     * @Route("/cashid")
     */
    function cashid(RequestStack $requestStack, CashIdService $CashId)
    {
        $cashRequest = $CashId->validate_request();
        $response = json_decode($CashId->confirm_request());

        if ($response->status === 0) {
            $session = $requestStack->getSession();
            $session->set('loggedIn', true);
            $session->set('address', $cashRequest['address']);
        }

        return $this->json($response);
    }

}

